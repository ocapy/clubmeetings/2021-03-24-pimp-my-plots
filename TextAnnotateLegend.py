# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 15:18:14 2021

@author: Ame
"""

from  matplotlib import pyplot as plt
import numpy as np

def example0():
    
    title="Text Annotation, Legend"
    fig,ax=plt.subplots() 
       
    ax.plot(x,y1,label="y1",ls="--")
    ax.scatter(x,y2,label="y2",marker="o",color="tab:orange") 
    ax.errorbar(x,y3,yerr=yerr,label="y3",color="tab:green") 
       
    leg1=ax.legend(title="The original Legend",loc="upper right")
  
    
    ## Modifying a legend (here we just reverse it)
    handles, labels = ax.get_legend_handles_labels()
    labels.reverse()
    handles.reverse()
    ax.legend(title="The reversed legend",handles=handles,labels=labels,loc="lower left")   

    # We add back the first legend as it has been replaced by the modified one
    ax.add_artist(leg1)
    

    #We show two red crosses where the text and the annoation will be
    ax.scatter(5,5,color="red",marker="+",s=200)
    ax.scatter(1,1,color="red",marker="+",s=200)
     
    #Both text and annotation can be heavily customize
    ax.text(5,5,"A text",ha="left",va="top",rotation=5,
            bbox = dict(boxstyle="sawtooth", fc="b",alpha=0.5)) 
    ax.annotate("An annotation",(1,1),xytext=(1,-2),ha="left",va="top",
                arrowprops={"facecolor":"black", "width":0.1},
                bbox = dict(boxstyle="round", fc="r",alpha=0.1))
   
    
    fig.suptitle(title,fontsize=30) 
    fig.tight_layout()
    fig.subplots_adjust(hspace=0.35)    
    fig.subplots_adjust(top=0.9)  
     
      

    
if __name__ == "__main__":
    
    
    # A simple dataset for the plot
    n=100
    x=np.linspace(0,10,n)
    y1=np.cos(x)*10+np.random.randn(n)*2
    y2=np.cos(x*1.1)*10+np.random.randn(n)*2    
    y3=np.cos(x*1.3)*10+np.random.randn(n)*2 
    yerr=np.random.randn(n)*0.5
        
    example0()
         
    #not necessary if you are using an IDE like Spyder
    plt.show()
