# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 15:18:14 2021

@author: Ame
"""

from  matplotlib import pyplot as plt
import numpy as np
import matplotlib.colors as mcolors


def errorFill(axe,X,Y,dY,color="k",alpha=1,zorder=0,line=True,smooth=1):
    Ys=Y
    if smooth!=1:      
        ker=np.ones(smooth)/smooth
        Ys=np.convolve(Y,ker,mode="same")
    XX=np.concatenate([X,np.flip(X)])
    YY=np.concatenate([Ys-dY,np.flip(Ys+dY)])
    axe.fill(XX,YY,zorder=zorder,alpha=alpha,color=color)
    if line :
        axe.plot(X,Y,zorder=zorder+1,color=color)
      

def example0():
    
    title="Types of matplotlib plots (a few are missing)"
    nx=3
    ny=4
    fig,ax=plt.subplots(ny,nx,figsize=(8,9)) 
   
    #Markers (symbols) types 
    #https://matplotlib.org/stable/api/markers_api.html   
    
    ax[0,0].plot(x,y,ls="-",color="tab:orange",ms=3,marker="s",markeredgecolor="red",markerfacecolor="pink")
    ax[0,0].set_title("plot")
    
    ax[0,1].scatter(x,y,marker="o",s=yerr*20,alpha=0.5,c=yerr,cmap=plt.cm.hot)
    ax[0,1].set_title("scatter")
    
    ax[0,2].errorbar(x,y,yerr=yerr,color="tab:green", elinewidth=2,errorevery=2,ecolor="tab:blue",linewidth=0.5)
    ax[0,2].set_title("errorbar")
    
    ax[1,0].imshow(zz,cmap=plt.cm.hot_r,extent=[x[0],x[-1],x[0],x[-1]])
    ax[1,0].set_title("imshow")   
    
    CS=ax[1,1].contour(xx,yy,zz,levels=[0.1,0.2,0.4,0.6,0.8],colors =["k","r","g","b","c"],linestyles=[":","-"])
    ax[1,1].clabel(CS, CS.levels, inline=True, fmt="%.1f", fontsize=8)
    ax[1,1].set_title("contour")  
 
    ax[1,2].contourf(xx,yy,zz,levels=[0.1,0.2,0.4,0.6,0.8,1.0],cmap=plt.cm.plasma)
    ax[1,2].set_title("contourf")     
    fig.suptitle(title) 
    
    ax[2,0].scatter(data[0],data[1],alpha=0.1)
    ax[2,0].set_title("scatter")    

    ax[2,1].hist(data[0],bins=20,range=[-6,6])
    ax[2,1].set_title("hist")    
   
    ax[2,2].hist2d(data[0],data[1],bins=20)
    ax[2,2].set_title("hist2d")     
  
    ax[3,0].bar(t-0.2, h1, 0.3)
    ax[3,0].bar(t-0.2, h2, 0.3,bottom=h1)
    ax[3,0].bar(t+0.2, h3, 0.3)
    ax[3,0].set_xticks(t)
    ax[3,0].set_xticklabels(days)
    ax[3,0].set_title("bar")     
 
    ax[3,1].pie( h1,labels=days,explode=explode,textprops={'fontsize': 8})
    ax[3,1].set_title("pie")       
 
    ax[3,2].fill(polyx,polyy,edgecolor="k",facecolor="tab:orange",hatch="////",zorder=2,alpha=0.8)
    ax[3,2].fill(polyx/2-5,polyy/2-5,edgecolor="k",facecolor="tab:blue",alpha=1,hatch="++++",zorder=1)
    ax[3,2].fill(polyx/2+5,polyy/2+5,edgecolor="k",facecolor="tab:green",alpha=1,hatch="oo",zorder=3)
    ax[3,2].set_title("fill")  
 
    
    fig.suptitle(title,fontsize=20) 
    fig.tight_layout()
    fig.subplots_adjust(hspace=0.35)    
    fig.subplots_adjust(top=0.9)  
    
    
def example1():
    
    #Using the plt.fill function I defined a custom errorFill function which 
    #I think improves the quality and readability of the figure

    title="Pimp your errobars with the fill function" 
    fig,ax=plt.subplots(1,2,figsize=(8,4)) 
    
    ax[0].errorbar(x,y,yerr=yerr,color="tab:green")
    ax[0].errorbar(x,y2,yerr=yerr2,color="tab:orange")
    ax[0].set_title("Classic errorbar")    
    
      
    errorFill(ax[1],x,y,yerr,color="tab:green",alpha=0.2)
    errorFill(ax[1],x,y2,yerr2,color="tab:orange",alpha=0.2)    
    ax[1].set_title("Custom errors with fill")
    
    fig.suptitle(title,fontsize=20) 
    fig.tight_layout()
    fig.subplots_adjust(hspace=0.35)    
    fig.subplots_adjust(top=0.8) 
     
def example2():
    
    title="imshow and contour on the same plot" 
    fig,ax=plt.subplots() 
    
    #The important things is to  set the data coordinates properly with the extent keyword) 
    ax.imshow(zz,cmap=plt.cm.hot_r,extent=[x[0],x[-1],x[0],x[-1]])     
    CS=ax.contour(zz,levels=[0.1,0.2,0.4,0.6,0.8,1.0],colors ="k",extent=[x[0],x[-1],x[0],x[-1]]) 
    #CS=ax.contour(xx,yy,zz,levels=[0.1,0.2,0.4,0.6,0.8,1.0],colors ="k")   
    ax.clabel(CS, CS.levels, inline=True, fmt="%.1f", fontsize=8)
    fig.suptitle(title,fontsize=20)     
    

    
def example3():
    
    title="imshow : Normalization, colorbars, zoom, interpolation" 
    fig,ax=plt.subplots(2,4,figsize=(12,6)) 
    
    #Note that the keywords fraction=0.046 and pad=0.04 in colorbar seems to magically
    #put it to the right scale compared to the plot whatever the size of the figure is.
    
    #Normalizations 
    im00 =ax[0,0].imshow(zz,cmap=plt.cm.hot_r,extent=[x[0],x[-1],x[0],x[-1]])     
    fig.colorbar(im00,ax=ax[0,0],fraction=0.046, pad=0.04)
    ax[0,0].set_title("Linear (min and min set by data)",fontsize=9)
    
    im01 =ax[0,1].imshow(zz,cmap=plt.cm.hot_r,extent=[x[0],x[-1],x[0],x[-1]],vmin=0, vmax=0.5)     
    #as we define a upper limit to the color dynamic it is good to show it in the colormap by adding an arrow with ,extend="max"
    fig.colorbar(im01,ax=ax[0,1],fraction=0.046, pad=0.04,extend="max")
    ax[0,1].set_title("Linear between 0 and 0.5",fontsize=9)
 
    im02 =ax[0,2].imshow(zz,cmap=plt.cm.hot_r,extent=[x[0],x[-1],x[0],x[-1]],norm=mcolors.PowerNorm(0.5, vmin=0, vmax=zz.max()))     
    fig.colorbar(im02,ax=ax[0,2],fraction=0.046, pad=0.04)
    ax[0,2].set_title("Power law (0.5,min,max)",fontsize=9)
    fig.suptitle(title)   
    
    im03 =ax[0,3].imshow(zz,cmap=plt.cm.hot_r,extent=[x[0],x[-1],x[0],x[-1]],norm=mcolors.LogNorm(vmin=zz.min(), vmax=zz.max()))     
    fig.colorbar(im03,ax=ax[0,3],fraction=0.046, pad=0.04)
    ax[0,3].set_title("Logarithmic (min,max)",fontsize=9)
    
    #Zoom
    im10 =ax[1,0].imshow(zz,cmap=plt.cm.hot_r,extent=[x[0],x[-1],x[0],x[-1]])     
    fig.colorbar(im10,ax=ax[1,0],fraction=0.046, pad=0.04)
    ax[1,0].set_title('"Squared" Zoom ',fontsize=9)
    ax[1,0].set_xlim(3,6)
    ax[1,0].set_ylim(4,7)
    
    im11 =ax[1,1].imshow(zz,cmap=plt.cm.hot_r,extent=[x[0],x[-1],x[0],x[-1]])     
    fig.colorbar(im11,ax=ax[1,1],fraction=0.046, pad=0.04)
    ax[1,1].set_title("Zoom on x",fontsize=9)
    ax[1,1].set_xlim(3,6)
  
    im12 =ax[1,2].imshow(zz,cmap=plt.cm.hot_r,extent=[x[0],x[-1],x[0],x[-1]],aspect="auto")     
    fig.colorbar(im12,ax=ax[1,2],fraction=0.046, pad=0.04)
    ax[1,2].set_title('Zoom on x (aspect="auto")',fontsize=9)
    ax[1,2].set_xlim(3,6)

    #interpolation
    im13 =ax[1,3].imshow(zz,cmap=plt.cm.hot_r,extent=[x[0],x[-1],x[0],x[-1]],interpolation="bilinear")     
    fig.colorbar(im13,ax=ax[1,3],fraction=0.046, pad=0.04)
    ax[1,3].set_title("Bilinear Interpolation turned on",fontsize=9)
    ax[1,3].set_xlim(3,6)
    ax[1,3].set_ylim(4,7)
    
    
    fig.suptitle(title,fontsize=20)  
    fig.tight_layout()
    fig.subplots_adjust(hspace=0.3)    
    fig.subplots_adjust(top=0.8)
    

    
if __name__ == "__main__":
    
    #Some 1D datasets for plot, scatter, errbor functions
    n=100
    x=np.linspace(0,10,n)
    y=np.cos(x)*10+np.random.randn(n)*2
    yerr=y*0+np.random.rand(n)*10
    y2=np.cos(x*1.2)*10+np.random.randn(n)*2
    yerr2=y*0+2+np.random.rand(n)
     
    # A Simple regular-grid 2D dataset (gaussian) for imshow and contour(f)
    xx,yy=np.meshgrid(x,x)
    r=np.sqrt((xx-5)**2+(yy-5)**2)
    zz=np.exp(-r**2/10)+np.random.rand(n,n)*0.005
    
    #A dataset for bar and pie plots
    days=["Lu","Ma","Me","Je","Ve","Sa","Di"]
    explode=[0,0,0,0,0,0.1,0.2]
    ndata=7
    t=np.arange(ndata)+1
    h1=np.random.rand(ndata)*10
    h2=np.random.rand(ndata)*3
    h3=np.random.rand(ndata)*5
    
    # A nice hexagon (because I'm French!)
    npt=6
    polyx=10*np.sin(2*np.pi*np.arange(npt)/npt)
    polyy=10*np.cos(2*np.pi*np.arange(npt)/npt)
    
    # A irregular 2D sample for scatter, hist, and hist2D plots 
    data = np.random.randn(2, 1000)
    
    example0()
    example1()
    example2()
    example3()   
    
    #not necessary if you are using an IDE like Spyder
    plt.show()
