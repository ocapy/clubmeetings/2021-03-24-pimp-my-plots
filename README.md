# 2021-03-24 Pimp My Plots

For the first meeting of our new OCApy club we focussed on python most used plotting library : **Matplotlib**

Here are the examples presented during the meeting:
- _FigureAndAxes.py_ : Figures and Axes (how to make plots and subplots)
- _Colors.py_ : Colors and Colormaps
- _PlotTypes.py_ : Plot types
- _TextAnnotateLegend_ : Text, annotations and legends

## links to resources that were brought up in the chat during the meeting:

- [Text rendering with LaTeX](https://matplotlib.org/stable/tutorials/text/usetex.html)
- [Additional scientific colourmaps](https://www.fabiocrameri.ch/colourmaps/) that can be installed with [pip](https://pypi.org/project/cmcrameri/)
- [Matplotlib animation tutorial](https://jakevdp.github.io/blog/2012/08/18/matplotlib-animation-tutorial/)
