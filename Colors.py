# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 15:18:14 2021

@author: Ame
"""

from  matplotlib import pyplot as plt
import numpy as np
import matplotlib.colors as mcolors
import matplotlib as mpl



def example0():
    #Names of the colors in matplotlib
    #https://matplotlib.org/3.1.0/gallery/color/named_colors.html
 
    plt.figure() 
    colors=[cname for cname in mcolors.BASE_COLORS];
    title="Base Colors in matplotlib"

    ncol=len(colors)        
    for i in range(ncol):
        col=colors[i]
        plt.plot(x,y+i,color=col)
        plt.text(np.mean(x),i,col,ha="center",va="center",color=col,backgroundcolor="w")
    plt.gca().get_xaxis().set_visible(False)
    plt.gca().get_yaxis().set_visible(False)
    plt.title(title) 
    plt.margins(0,0)

def example1():
    
    title="Some of the CCS4 colors"
    plt.figure() 
    
    colors=[cname for cname in mcolors.CSS4_COLORS]  
    ncol=len(colors)
        
    for i in range(20):
        col=colors[np.random.randint(0,ncol)]
        plt.plot(x,y+i,color=col)
        plt.text(np.mean(x),i,col,ha="center",va="center",color=col,backgroundcolor="w")
    plt.gca().get_xaxis().set_visible(False)
    plt.gca().get_yaxis().set_visible(False)
    plt.title(title) 
    plt.margins(0,0)


def example2():
    
    title="The default color cycle (10 values)"
    plt.figure() 
 
    for i in range(13):
        plt.plot(x,y+i)
        plt.text(np.mean(x),i,i,ha="center",va="center")
    plt.gca().get_xaxis().set_visible(False)
    plt.gca().get_yaxis().set_visible(False)
    plt.title(title) 
    plt.margins(0,0)

def example3():
    
    title="A custom color (an style) cycle with 4 values"
    plt.figure() 
    
    #rcParams contains a lot of defaut presets including the color cycle
    plt.rcParams['axes.prop_cycle'] = plt.cycler(color=["r", "g", "b","c"]) 
    
    # Here we replace a color cycle by a linestyle cycle (intersting for b/w figures)
    plt.rcParams['axes.prop_cycle'] = plt.cycler(ls=["-",":","--","-."]) 
    
    # Of course you can combine both
    plt.rcParams['axes.prop_cycle'] = plt.cycler(ls=["-",":","--","-."],color=["r", "g", "b","c"]) 

    for i in range(13):
        plt.plot(x,y+i)
        plt.text(np.mean(x),i,i,ha="center",va="center")
    plt.gca().get_xaxis().set_visible(False)
    plt.gca().get_yaxis().set_visible(False)
    plt.title(title) 
    plt.margins(0,0)
  
    #Reset to befault rcParams
    plt.style.use('default')
    
def example4():
    
    #Matplotlib Colormaps list
    #https://matplotlib.org/3.3.2/tutorials/colors/colormaps.html
    
    title="Using a colormap with plt.plot"
    plt.figure() 
 
    for i in range(13):
        # We use the hot colormap and call it with a value between 0 and 1
        coli=plt.cm.hot(i/13.)
        plt.plot(x,y+i,color=coli)
        plt.text(np.mean(x),i,i,ha="center",va="center")
    plt.gca().get_xaxis().set_visible(False)
    plt.gca().get_yaxis().set_visible(False)
    plt.title(title) 
    plt.margins(0,0)
    
    
def example5():
    
  
    title="Using colormaps with plt.imshow"
    plt.figure() 
 
    plt.imshow(zz,cmap=plt.cm.hot)
    plt.colorbar()
    plt.gca().get_xaxis().set_visible(False)
    plt.gca().get_yaxis().set_visible(False)
    plt.title(title) 
   
    
def example6():
    
    title="Creating a custom colormap"
    plt.figure() 

    #Here we will create a colormap linearly interpolated betwen 3 colors :
    #red (1,1,1) at 0, green (0,1,0) at 0.5 and blue (0,0,1) at 1. 
    
    #The dictionary contains the red, green and bleu keys
    cdict = {'red':   [(0.0,  1.0, 1.0), # we define that at 0 red is 1 (the last parameter is only used if  ther is a discontinuity)
                       (0.5,  0.0, 0.0),
                       (1.0,  0.0, 0.0)],

         'green': [(0.0,  0.0, 0.0),
                   (0.5, 1.0, 1.0),
                   (1.0,  0.0, 0.0)],

         'blue':  [(0.0,  0.0, 0.0),
                   (0.5,  0.0, 0.0),
                   (1.0,  1.0, 1.0)]}
    

    cmap=mcolors.LinearSegmentedColormap('mycmap',cdict,256)
    
   
    plt.imshow(zz,cmap=cmap)
    plt.colorbar()
    plt.gca().get_xaxis().set_visible(False)
    plt.gca().get_yaxis().set_visible(False)
    plt.title(title) 
        
  
if __name__ == "__main__":
    
    #A simple 1D x,y dataset  for plot function
    x=np.linspace(0,100,1000)
    y=np.cos(x)*0.5
    
    # a 2D dataset for imshow function
    xx,yy=np.meshgrid(x,x)
    r=np.sqrt((xx-50)**2+(yy-50)**2)
    zz=np.cos(r)
   
    example0()
    example1()
    example2()
    example3()   
    example4()
    example5()  
    
    #not necessary if you are using an IDE like Spyder
    plt.show()
