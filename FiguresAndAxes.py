# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 15:18:14 2021

@author: Ame
"""

from  matplotlib import pyplot as plt
import numpy as np
from matplotlib.gridspec import GridSpec

def example0():   
    title="Matplotlib most simple plot"
    plt.figure() 
    plt.plot(x,y)
    plt.title(title)
    plt.xlabel("x")
    plt.ylabel("y")
    
    #setting plots limits
    #plt.xlim(0,150)
    #plt.ylim(-1,2)
    
    #Alternative to xlim et ylim settings margins in data coordinates
    #plt.margins(0,0)
       
def example1():
    
    title="The figure and axes objects"   
    fig,ax=plt.subplots()   
    ax.plot(x,y)  
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_title(title)
    ax.set_xlim(0,150)
    ax.set_ylim(-1,2)
    
    #Getting the references to the current (active) figure and axes objects
    fig=plt.gcf()
    ax=plt.gca()
    
    
def example2():
    
    title="Multiple plots with subplots(nrows,ncols)"
    nrows=2
    ncols=3
    
    #Here we share the x axis of all plots but the y are shared by row
    fig,ax=plt.subplots(nrows,ncols,sharex=True,sharey="row")
  
    for irow in range(nrows):
        for icol in range(ncols):
            ax[irow,icol].plot(x,y)  
            ax[irow,icol].set_xlabel("x")
            ax[irow,icol].set_ylabel("y{0}".format(irow))
            ax[irow,icol].set_title("Title {0}-{1}".format(irow,icol))
             
            #Hiding the useless axis label
            #Note the get_yaxis() and .get_xaxis() return and axis object
            if icol!=0:
                ax[irow,icol].get_yaxis().set_visible(False)
            if irow!=1:
                ax[irow,icol].get_xaxis().set_visible(False)  
                
    ##Removing spaces between plots
    fig.subplots_adjust(wspace=0.02,hspace=0.15)    

            
     ##A super title for the whole figure!
    fig.suptitle(title)
            

       
    #Adjust the padding between and around subplots
    fig.tight_layout()
    
    ## Tight_layout doesn't take into account the suptitle so we need to add some space above
    fig.subplots_adjust(top=0.85)
    
def example3():    
    
    title="Multiple plots with subplot"
    fig=plt.figure()
    
    #subplot params are the following : nrow,ncol,position of the plot in the grid
    ax1=plt.subplot(2,1,2) # create the second subplot of on a 2-rows 1-col grid 
    ax1.set_title("subplot(2,1,2)")
    ax2=plt.subplot(2,2,1) # create the first subplot of on a 2-rows 2-cols grid 
    ax2.set_title("subplot(2,2,1)")
    ax3=plt.subplot(2,2,2) # create the second subplot of on a 2-rows 2-cols grid
    ax3.set_title("subplot(2,2,2)")
     
    fig.suptitle(title)
    fig.tight_layout()
    fig.subplots_adjust(top=0.85)
    
def example4():    
    
    title="GridSpec : mulitple subplots on a flexible grid"
    fig = plt.figure()

    #Creating a 3rows x 3cols grid
    gs = GridSpec(3, 3, figure=fig)
    # Add a plot on the first row and taking all the cols 
    ax1 = fig.add_subplot(gs[0, :]) 
    ax1.set_title("gs[0, :]")
     # Add a plot on the second row and taking all but the last the cols 
    ax2 = fig.add_subplot(gs[1, :-1])
    ax2.set_title("gs[1, :-1]")
     # Add a plot on the two last rows (1,2) and the last  col 
    ax3 = fig.add_subplot(gs[1:, -1]) 
    ax3.set_title("gs[1, :-1]")
    # Add a plot on the last row first col
    ax4 = fig.add_subplot(gs[-1, 0])  
    ax4.set_title("gs[-1, :0]")
    # Add a plot on the last row and the penultimate col
    ax5 = fig.add_subplot(gs[-1, -2])
    ax5.set_title("gs[-1, -2]")
    
    fig.tight_layout()
    fig.suptitle(title)
    fig.subplots_adjust(top=0.9)

 
def example5():  
    title="Subplots with custom position using add_axes"
    fig = plt.figure()
    
    ax0=plt.axes()
    ax0.set_title("ax0")
    #coordinates are in figure space from 0 to 1. Params are :x,y width,height
    ax1=fig.add_axes([0.6,0.6,0.2,0.2])  
    ax1.set_title("ax1")
    ax2=fig.add_axes([0.2,0.2,0.2,0.2])
    ax2.set_title("ax2")
    
    fig.suptitle(title)
    
    
if __name__ == "__main__":
    
    #A simple x,y dataset for the plots
    x=np.linspace(0,30,1000)
    y=np.cos(x)
    
    example1()    
    example2()   
    example3()    
    example4()    
    example5()
    
    #not necessary if you are using an IDE like Spyder
    plt.show()
    
